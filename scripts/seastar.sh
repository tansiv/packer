#!/bin/sh -eux



mkdir -p /opt
cd /opt

# install tools
# wrk2
cd /opt
sudo dnf install -y memcached llvm clang gcc cmake bison flex
sudo dnf install -y openssl-devel zlib-devel
# git clone https://github.com/giltene/wrk2.git
# cd wrk2
# make

# Libmemcached
cd /opt
git clone https://gitlab.inria.fr/lcossero/libmemcached
cd libmemcached
mkdir build-libmemcached && cd build-libmemcached
cmake ..
make


# Let's install and compile seastar
# Seastar
cd /opt

git clone --recurse-submodules https://gitlab.inria.fr/tansiv/seastar.git

cd seastar

./install-dependencies.sh
./configure.py --mode=release --enable-dpdk
ninja -C build/release