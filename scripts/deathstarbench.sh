#!/bin/sh -eux

apt -y install python3-aiohttp \
    libssl-dev \
    libz-dev \
    luarocks

luarocks install luasocket

# Let's pull all required docker images (because no internet access in a VM)
docker pull jaegertracing/jaeger-agent:latest
docker pull deathstarbench/social-network-microservices:latest
docker pull redis
docker pull yg397/media-frontend:xenial
docker pull memcached
docker pull mongo:4.4.6
docker pull deathstarbench/social-network-microservices:latest
docker pull yg397/openresty-thrift:xenial
docker pull jaegertracing/jaeger-collector
docker pull jaegertracing/jaeger-query
docker pull jaegertracing/jaeger-cassandra-schema

mkdir -p app
cd app
git clone https://github.com/delimitrou/DeathStarBench.git