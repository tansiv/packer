#!/bin/sh -eux

apt-get -y install build-essential meson ninja-build python3-pyelftools libnuma-dev git python3-pip gawk gcc make libssl-dev redis

mkdir -p /opt/f-stack

git clone https://github.com/F-Stack/f-stack.git /opt/f-stack

pip3 install pyelftools --upgrade

# Use gawk and not mawk
update-alternatives --set awk /usr/bin/gawk

# Compile DPDK
cd /opt/f-stack/dpdk
meson -Denable_kmods=true build
ninja -C build
ninja -C build install


# Compile F-stack
export FF_PATH=/opt/f-stack
export PKG_CONFIG_PATH=/usr/lib64/pkgconfig:/usr/local/lib64/pkgconfig:/usr/lib/pkgconfig
cd /opt/f-stack/lib/
make

# Install F-stack
make install

# Compile and install redis
cd ..
cd app/redis-6.2.6/deps/jemalloc
./autogen.sh 
cd ../..
make
make install