#!/bin/sh -eux

# We install and build dpdk manually to get all example applications
apt-get -y install build-essential meson ninja-build python3-pyelftools libnuma-dev git

package="dpdk-22.11.1.tar.xz"
echo "0594708fe42ce186a55b0235c6e20cfe  dpdk-22.11.1.tar.xz" > "$package".md5
wget "http://fast.dpdk.org/rel/dpdk-22.11.1.tar.xz" && md5sum -c "$package".md5 && tar -xvf "$package"

cd dpdk-stable-22.11.1
meson setup -Dplatform=generic -Dexamples=all build
cd build
ninja
ninja install
ldconfig
