#!/bin/sh -eux

dnf -y update

# some utils

dnf -y install wget vim git automake texinfo tmux

# some apps to validate the bouzin
# since we have ssh ready on the management interface, we could also install
# some apps on the fly
dnf -y install \
    fping \
    netperf \
    iperf3 \
	iptables \
	stress \
    flent \
    python-matplotlib \
    python-setuptools \
    redis \
    ethtool

mkdir -p /home/tansiv/.ssh
ssh-keygen -t rsa -f /home/tansiv/.ssh/id_rsa -P ''
cat /home/tansiv/.ssh/id_rsa.pub >> /home/tansiv/.ssh/authorized_keys
chown tansiv:tansiv -R /home/tansiv/.ssh

# coremark
dnf -y install gcc g++ make perf
cd /opt && git clone https://github.com/eembc/coremark.git
# build the executable (single-threaded)
cd /opt/coremark && make compile

rm -rf /usr/share/doc/*

find /var/cache -type f -exec rm -rf {} \;
find /var/log/ -name *.log -exec rm -f {} \;


systemctl disable firewalld
