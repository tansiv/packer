#!/bin/sh -eux

apt -y update

# some utils
apt-get -y install software-properties-common
apt-add-repository non-free
apt-get -y update

apt-get -y install wget vim git autotools-dev automake texinfo tmux

# some apps to validate the bouzin
# since we have ssh ready on the management interface, we could also install
# some apps on the fly
apt-get -y install taktuk \
    fping \
    netperf \
    iperf3 \
	iptables \
	stress \
    flent \
    python3-matplotlib \
    python3-setuptools \
    redis-server \
    ethtool

# Instrumented ping
apt-get install -y --no-install-recommends \
	clang \
	docbook-xsl-ns \
	file \
	gcc \
	gettext \
	iproute2 \
	libcap-dev \
	libidn2-0-dev \
	libssl-dev \
	make \
	meson \
	pkg-config \
	xsltproc

# linpack
# package='l_mklb_p_2018.3.011.tgz'
# mkdir /opt/linpack
# cd /opt/linpack
# echo "eac1fc2784c3ac2f130dafaa8e590b48f7623c577b469bae8b53decd27652b53  l_mklb_p_2018.3.011.tgz" > "$package".sha256
# wget "http://registrationcenter-download.intel.com/akdlm/irc_nas/9752/$package" && sha256sum -c "$package".sha256 && tar -xzf "$package"

# coremark
cd /opt && git clone https://github.com/eembc/coremark.git

# nbench
package='nbench-byte-2.2.3.tar.gz'
mkdir /opt/nbench
cd /opt/nbench
echo "723dd073f80e9969639eb577d2af4b540fc29716b6eafdac488d8f5aed9101ac  nbench-byte-2.2.3.tar.gz" > "$package".sha256
wget "http://www.math.utah.edu/~mayer/linux/$package" && sha256sum -c "$package".sha256 && tar -xzf "$package"

# libmemcached
cd /opt
git clone https://gitlab.inria.fr/lcossero/libmemcached.git
apt-get -y install cmake clang-9 gcc-9 bison flex
cd libmemcached
mkdir build-libmemcached
cd build-libmemcached
cmake ..
make

# bmc
cd /opt
git clone https://gitlab.inria.fr/lcossero/bmc-cache.git
cd bmc-cache
apt-get -y install gpg curl tar make flex bison libssl-dev libelf-dev xz-utils bc llvm
./kernel-src-download.sh
./kernel-src-prepare.sh
cd bmc
make

mkdir -p /home/tansiv/.ssh
ssh-keygen -t rsa -f /home/tansiv/.ssh/id_rsa -P ''
cat /home/tansiv/.ssh/id_rsa.pub >> /home/tansiv/.ssh/authorized_keys
chown tansiv:tansiv -R /home/tansiv/.ssh

# boot fast (we're in icount mode most lickely so make the boot faster)
sed -i -e 's/^GRUB_TIMEOUT=[0-9]\+$/GRUB_TIMEOUT=0/' /etc/default/grub
update-grub


apt-get -y purge libx11-data xauth libxmuu1 libxcb1 libx11-6 libxext6
apt-get -y purge ppp pppconfig pppoeconf
apt-get -y purge popularity-contest

apt-get -y autoremove
apt-get -y clean;

rm -rf /usr/share/doc/*

find /var/cache -type f -exec rm -rf {} \;
find /var/log/ -name *.log -exec rm -f {} \;

