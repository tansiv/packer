#!/bin/sh -eux


docker pull cassandra:4

cd /opt
curl -O --location https://github.com/brianfrankcooper/YCSB/releases/download/0.17.0/ycsb-0.17.0.tar.gz
tar xfvz ycsb-0.17.0.tar.gz

# removing non essential bindings
cd ycsb-0.17.0
mv cassandra-binding /tmp
rm -rf *-binding
mv /tmp/cassandra-binding .

# install python 2.7 for ycsb
# install java as well
dnf install -y python2.7 java-11-openjdk java-11-openjdk-devel